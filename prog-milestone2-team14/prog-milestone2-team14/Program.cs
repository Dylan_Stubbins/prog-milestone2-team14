﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team14
{
    class Program
    {
        static void Main(string[] args)
        {
            int menuchoice = 0;
            while (menuchoice != 5)
            {

                Console.WriteLine("MENU");
                Console.WriteLine("Please enter the number that you want to do:");
                Console.WriteLine("1. Date Calculator");
                Console.WriteLine("2. Grade Average");
                Console.WriteLine("3. Generate a Random Number");
                Console.WriteLine("4. Rate Favourite Food");
                Console.WriteLine("5. Exit");

                menuchoice = int.Parse(Console.ReadLine());

                switch (menuchoice)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Hello Can I please get your age so I can work out how old you are in mm/dd/yy please");
                        var date = Console.ReadLine();
                        DateTime dateConvert = Convert.ToDateTime(date);
                        GetAge(dateConvert);
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Taylors Method here");
                        var data = Methodthreeone();
                        menuchoice = 0;
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Dylan Method here");
                        guessingGame();
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Sam method here");
                        FoodRank();
                        break;
                    case 5:
                        Console.Clear();
                        Console.ReadLine();
                        break;
                    default:
                        Console.WriteLine("Sorry, invalid selection");
                        break;
                }

            }
        }
        public static int GetAge(DateTime birthDate)
        {
            DateTime n = DateTime.Now; // To avoid a race condition around midnight
            int age = n.Year - birthDate.Year;

            if (n.Month < birthDate.Month || (n.Month == birthDate.Month && n.Day < birthDate.Day))
                age--;

            return age;
        }

        public static Dictionary<string, Tuple<double, string>> Methodthreeone()
        {
            Console.WriteLine("Are you doing DAC 5 or DAC 6? Please enter 5 for 5 and 6 for 6");
            var level = int.Parse((Console.ReadLine()));
            Console.WriteLine("What's your student id number?");
            var studentId = Console.ReadLine();
            var dacMarks = new Dictionary<string, Tuple<double, string>>();

            {
                {

                    switch (level)
                    {
                        case 5:
                            Console.WriteLine("Please enter your course paper code and then marks for your 4 papers");
                            var dacFiveNumberOfCourses = 4;
                            var dacFiveMarks = new Dictionary<string, Tuple<double, string>>();
                            for (int i = 1; i < dacFiveNumberOfCourses; i++)
                            {
                                dacFiveMarks.Add(Console.ReadLine(), Methodthreethree());
                            }
                            dacMarks = dacFiveMarks;
                            Methodthreefour(dacMarks);
                            foreach (KeyValuePair<string, Tuple<double, string>> entry in dacMarks)
                            {
                                Console.WriteLine("Course = {0}, Mark Percentage = {1}, Letter Grade = {2}", dacMarks.Keys, dacMarks.Values, dacMarks.Values);
                            }
                            break;

                        case 6:
                            Console.WriteLine("Please enter the course paper code and then  marks for your 3 papers");
                            var dacSixNumberOfCourses = 3;
                            var dacSixMarks = new Dictionary<string, Tuple<double, string>>();
                            for (int i = 1; i < dacSixNumberOfCourses; i++)
                            {
                                dacSixMarks.Add(Console.ReadLine(), Methodthreethree());
                            }
                            dacMarks = dacSixMarks;
                            Methodthreefour(dacMarks);
                            foreach (KeyValuePair<string, Tuple<double, string>> entry in dacMarks)
                            {
                                Console.WriteLine("Course = {0}, Mark Percentage = {1}, Letter Grade = {2}", dacMarks.Keys, dacMarks.Values);
                            }
                            break;
                        default:
                            Console.WriteLine("Please input a correct Value");
                            break;
                    }
                    return dacMarks;
                }

            }
        }
        public static Tuple<double, string> Methodthreethree()
        {
            var percentageScore = double.Parse(Console.ReadLine());
            string letterGradeScore;
            if (percentageScore < 90)
            {
                letterGradeScore = "A+";
            }
            else if (percentageScore < 85)
            {
                letterGradeScore = "A";
            }
            else if (percentageScore < 80)
            {
                letterGradeScore = "A-";
            }
            else if (percentageScore < 75)
            {
                letterGradeScore = "B+";
            }
            else if (percentageScore < 70)
            {
                letterGradeScore = "B";
            }
            else if (percentageScore < 65)
            {
                letterGradeScore = "B-";
            }
            else if (percentageScore < 60)
            {
                letterGradeScore = "C+";
            }
            else if (percentageScore < 55)
            {
                letterGradeScore = "C";
            }
            else if (percentageScore < 50)
            {
                letterGradeScore = "C-";
            }
            else if (percentageScore < 40)
            {
                letterGradeScore = "D";
            }
            else
            {
                letterGradeScore = "E";
            }
            var Tuple = new Tuple<double, string>(percentageScore, letterGradeScore);
            return Tuple;
        }
        public static void Methodthreefour(Dictionary<string, Tuple<double, string>> dacMarks)
        {
            var data = dacMarks;
            double average = 0;
            int counter = 0;
            foreach (KeyValuePair<string, Tuple<double, string>> entry in data)
            {
                average += entry.Value.Item1;
                counter++;
            }
            average = average / counter;
            Console.WriteLine($"Your average is {average}");
        }
        public static void Methodthreefive(Dictionary<string, Tuple<double, string>> dacMarks)
        {
            var data = dacMarks;
            var aPlusList = new List<string>();
            foreach (KeyValuePair<string, Tuple<double, string>> entry in data)
            {
                if (entry.Value.Item1 > 85)
                {
                    aPlusList.Add(entry.Key);
                }

            }
            foreach (var x in aPlusList)
            {
                Console.WriteLine($"{x} has passed with an A+");
            }
            Console.ReadKey();
        }

























































































        static void Dylans()
        {
            guessingGame();
        }

        static int numberOfGames = 0;
        static List<Tuple<int, int>> gamesPlayed = new List<Tuple<int, int>>();

        static void guessingGame()
        {
            numberOfGames += 1;
            var answer = 0;
            var score = 0;
            {
                Console.WriteLine("I'm thinking of a number between 1-5. You have 5 attempts to guess the correct numbers.\nEnter in a number:");
                for (int i = 0; i <= 4; i++)
                {

                    Random r = new Random();
                    answer = (r.Next(1, 5));

                    var guess = Convert.ToInt32(Console.ReadLine());
                    if (guess == answer)
                    {
                        Console.WriteLine("Correct.");
                        score += 1;
                    }
                    else if (guess != answer)
                    {
                        Console.WriteLine("Try again.", answer);
                    }

                }
                Console.WriteLine($"\nYou guessed {score} out of 5 correct.");

                gamesPlayed.Add(Tuple.Create(numberOfGames, score));

            }
            replay(score);
        }

        static void replay(int score)
        {
            Console.WriteLine("\nWould you like to play again? (y/n)");
            var a = Console.ReadLine();
            if (a == "y")
            {
                Console.Clear();
                guessingGame();
            }
            else if (a == "n")
            {
                Console.Clear();
                Console.WriteLine("Here is a list of your previous scores:\n");
                finalScores();

                Console.WriteLine("\nThankyou. See you next time.");
            }
        }
        static void finalScores()
        {

            foreach (var x in gamesPlayed)
            {
                Console.WriteLine($"Game {x.Item1} - you scored {x.Item2}/5");
            }
        }



















































        static void FoodRank()
        {
            Ranking();
            Lengthcheck(Ranking());
            Printorder(Ranking());
            ChangeFood(Ranking());
        }
        static Dictionary<int, string> rank = new Dictionary<int, string>();
        static Dictionary<int, string> Ranking()
        {
            var i = 0;
            var count = 5;
            Console.WriteLine("Please enter your 5 favourite foods. Enter the rank and then food eg. 1<enter> Pizza<enter>");
            if (rank.Count == 0)
            {
                for (i = 0; i < count; i++)
                {
                    rank.Add(int.Parse(Console.ReadLine()), Console.ReadLine());
                }
            }
            return rank;
        }
        static Dictionary<int, string> Lengthcheck(Dictionary<int, string> rank)
        {
            if (rank.Count != 5)
            {
                rank = Ranking();
            }
            return rank;
        }
        static void Printorder(Dictionary<int, string> rank)
        {
            var value = new List<string>();
            for (int i = 0; i <= rank.Count; i++)
            {
                if (rank.ContainsKey(i))
                {
                    value.Add(rank[i]);
                }
            }
            foreach (var x in value)
            {
                Console.WriteLine(x);
            }
        }
        static Dictionary<int, string> ChangeFood(Dictionary<int, string> rank)
        {
            foreach (var x in rank)
            {
                Console.WriteLine($"{x.Key}, {x.Value}");
            }
            Console.WriteLine($"Please enter change to change food in the list.");
            var choice = 0;
            var select = Console.ReadLine();
            select = select.ToLower();
            while (choice != 1)
            {
                switch (select)
                {
                    case "change":
                        do
                        {
                            Console.WriteLine("Please enter a number to delete that entry");
                            rank.Remove(int.Parse(Console.ReadLine()));
                            Console.WriteLine("Please enter a food into the rank removed eg 1<enter> Pizza<enter>");
                            rank.Add(int.Parse(Console.ReadLine()), Console.ReadLine());
                            Console.WriteLine("Enter change to change another ranking or enter exit");
                            select = Console.ReadLine();
                            select = select.ToLower();
                        }
                        while (select == "change");
                        break;
                    case "exit":
                        choice = 1;
                        Console.WriteLine("the programme is now finished");
                        break;
                    default:
                        Console.WriteLine("Please enter add or delete");
                        select = Console.ReadLine();
                        break;
                }
            }
            foreach (var x in rank)
            {
                Console.WriteLine($"{x.Key}, {x.Value}");
            }
            return rank;
        }
    }
}























































